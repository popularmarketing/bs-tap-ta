<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <aside id="text-3" class="widget widget_text"><h3 class="widget-title"><span>RÓLUNK</span></h3>			<div class="textwidget"><p><h1 style="color:white;">BS Tapéta Kft.
                        </h1><br />
                        A BS Tapéta Kft. Kelet-Magyarország legnagyobb tapéta választékával várja vásárlóit exkluzív debreceni belvárosi bemutatótermében.  </p>
                    </div>
                </aside>                    <div class="social">
                    <div class="social-menu"><ul id="social-menu" class="menu"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-97"><a href="http://facebook.com/">facebook</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-98"><a href="http://twitter.com/">twitter</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-171"><a href="http://linkedin.com/">linkedin</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-172"><a href="http://dribbble.com/">dribbble</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-173"><a href="http://youtube.com/">youtube</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-174"><a href="feed/index.html">feed</a></li>
                        </ul></div>                    </div>
            </div>
            <div class="col-md-4">
                <aside id="better-menu-widget-2" class="widget better-menu-widget"><h3 class="widget-title"><span>Hasznos oldalak</span></h3><div class="menu-information-container"><ul id="menu-information" class="menu"><li id="menu-item-276" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-4 current_page_item menu-item-276"><a href="fooldal">Nyitólap</a></li>
                            <li id="menu-item-99" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-99"><a href="modern_tapeta">Modern Tapéta</a></li>
                            <li id="menu-item-100" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-100"><a href="design_tapeta">Design Tapéta</a></li>
                            <li id="menu-item-101" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-101"><a href="klasszikus_tapeta">Klasszikus Tapéta</a></li>
                            <li id="menu-item-102" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-102"><a href="natrual_tapeta">Natrual Tapéta</a></li>
                            <li id="menu-item-273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-273"><a href="amerikai_tapeta">Amerikai Tapéta</a></li>
                            <li id="menu-item-274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-274"><a href="bemutatoterem">Bemutatóterem</a></li>
                            <li id="menu-item-275" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-275"><a href="kapcsolat">Kapcsolat</a></li>
                        </ul></div></aside>                </div>
            <div class="col-md-4">
                <aside id="text-4" class="widget widget_text"><h3 class="widget-title"><span>KAPCSOLATTARTÁS</span></h3>			<div class="textwidget"><div class="office">
                            <p><i class="fa fa-map-marker"></i> <?php echo $beallitasok->uzletcim?>
                            </p>
                            <p><i class="fa fa-phone"></i> <?php echo $beallitasok->mobil?> </p>
                            <p><i class="fa fa-envelope"></i> <?php echo $beallitasok->nyilvanosemail?> </p>
                        </div></div>
                </aside>                </div>
        </div>
    </div>
</footer><!-- .site-footer -->
<div class="copyright">
    <div class="container center">
        &copy; 2016 Minden jog fenntartva - <a href="http://popularmarketing.hu/" target="_blank">Weboldal készítés </br><img src="img/logo.png" alt="PopularMarketing"></a>
    </div><!-- .copyright -->
</div><!-- #page -->

<a class="scrollup"><i class="fa fa-angle-up"></i></a>

<script>
    jQuery(document).ready(function ($) {
        $("#site-navigation").headroom(
            {
                offset: 160
            }
        );
    });
</script>

<script>
    jQuery(document).ready(function ($) {
        var $window = $(window);
        // Scroll up
        var $scrollup = $('.scrollup');

        $window.scroll(function () {
            if ($window.scrollTop() > 100) {
                $scrollup.addClass('show');
            } else {
                $scrollup.removeClass('show');
            }
        });

        $scrollup.on('click', function (evt) {
            $("html, body").animate({scrollTop: 0}, 600);
            evt.preventDefault();
        });
    });
</script>

<script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
        var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
        errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
        errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
        errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
        errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
</script>
<link rel='stylesheet' id='font-awesome-css'  href='wp-content/plugins/js_composer/assets/lib/bower/font-awesome/css/font-awesome.min2720.css?ver=4.11.2' type='text/css' media='all' />
<link rel='stylesheet' id='vc_google_fonts_abril_fatfaceregular-css'  href='http://fonts.googleapis.com/css?family=Abril+Fatface%3Aregular&amp;subset=latin&amp;ver=4.5' type='text/css' media='all' />
<link rel='stylesheet' id='themepunchboxextcss-css'  href='wp-content/plugins/essential-grid/public/assets/css/lightbox5bfb.css?ver=2.0.9.1' type='text/css' media='all' />
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var _wpcf7 = {"loaderUrl":"http:\/\/woodworker.thememove.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","recaptchaEmpty":"Please verify that you are not a robot.","sending":"Sending ..."};
    /* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scriptsa94e.js?ver=4.4.1'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/jQuery.headroom.minf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/headroom.minf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/snap.minf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/owl.carousel.minf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/jquery.counterup.minf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/waypoints.minf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/tm-wood-worker/js/mainf488.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-includes/js/comment-reply.min11b8.js?ver=4.5'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min11b8.js?ver=4.5'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min2720.js?ver=4.11.2'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min2720.js?ver=4.11.2'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/waypoints/waypoints.min2720.js?ver=4.11.2'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/vc_accordion/vc-accordion.min2720.js?ver=4.11.2'></script>
<script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/vc-tta-autoplay/vc-tta-autoplay.min2720.js?ver=4.11.2'></script>

</body>
