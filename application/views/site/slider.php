<div id="content" class="site-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <article id="post-4">
                        <div class="entry-content">
                            <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1438158225152 vc_row-no-padding"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460530622864"><div class="wpb_wrapper"><div class="wpb_revslider_element wpb_content_element"><link href="http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800" rel="stylesheet" property="stylesheet" type="text/css" media="all" /><link href="http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900" rel="stylesheet" property="stylesheet" type="text/css" media="all" /><link href="http://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                                                <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#c69453;padding:0px;margin-top:0px;margin-bottom:0px;">
                                                    <!-- START REVOLUTION SLIDER 5.2.4.1 auto mode -->
                                                    <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.2.4.1">
                                                        <ul>	<!-- SLIDE  -->
                                                            <?php foreach($slider->result() as $row){?>
                                                                <li data-index="rs-1" data-transition="parallaxtoright" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://woodworker.thememove.com/wp-content/uploads/2015/07/s11_bg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                                    <!-- MAIN IMAGE -->
                                                                    <img src="assets/uploads/slider/<?php echo $row->file?>"  alt="" title="s11_bg"  width="1920" height="680" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                                                    <!-- LAYERS -->

                                                                    <!-- LAYER NR. 1 -->
                                                                    <div class="tp-caption TM-Text-01   tp-resizeme"
                                                                         id="slide-1-layer-4"
                                                                         data-x="['left','left','left','left']" data-hoffset="['982','982','982','982']"
                                                                         data-y="['top','top','top','top']" data-voffset="['285','285','285','285']"
                                                                         data-width="none"
                                                                         data-height="none"
                                                                         data-whitespace="nowrap"
                                                                         data-visibility="['on','on','on','off']"
                                                                         data-transform_idle="o:1;"

                                                                         data-transform_in="x:right;skX:-85px;s:1000;e:Power4.easeInOut;"
                                                                         data-transform_out="opacity:0;s:300;"
                                                                         data-start="1920"
                                                                         data-splitin="none"
                                                                         data-splitout="none"
                                                                         data-responsive_offset="on"


                                                                         style="z-index: 8; white-space: nowrap;font-family:Arial;text-transform:left;"><?php echo $row->nev?></div>
                                                                </li>
                                                            <?php }?>
                                                        </ul>
                                                        <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
                                                            if(htmlDiv) {
                                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                            }else{
                                                                var htmlDiv = document.createElement("div");
                                                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                            }
                                                        </script>
                                                        <div class="tp-bannertimer" style="height: 5px; background-color: rgba(198, 148, 83, 0.80);"></div>	</div>
                                                    <script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.TM-Text-01,.TM-Text-01{color:rgba(255,255,255,1.00);font-size:60px;line-height:60px;font-weight:700;font-style:normal;font-family:;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px}.tp-caption.TM-Button,.TM-Button{color:rgba(0,0,0,1.00);font-size:13px;line-height:18px;font-weight:700;font-style:normal;font-family:;padding:0px 0px 0px 0px;text-decoration:none;text-align:left;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px;letter-spacing:2px;border:none !important}";
                                                        if(htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        }else{
                                                            var htmlDiv = document.createElement("div");
                                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                    <script type="text/javascript">
                                                        /******************************************
                                                         -	PREPARE PLACEHOLDER FOR SLIDER	-
                                                         ******************************************/

                                                        var setREVStartSize=function(){
                                                            try{var e=new Object,i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                                                                e.c = jQuery('#rev_slider_1_1');
                                                                e.responsiveLevels = [1240,1240,1240,480];
                                                                e.gridwidth = [1920,1024,778,480];
                                                                e.gridheight = [680,768,960,720];

                                                                e.sliderLayout = "auto";
                                                                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})

                                                            }catch(d){console.log("Failure at Presize of Slider:"+d)}
                                                        };

                                                        setREVStartSize();

                                                        var tpj=jQuery;
                                                        tpj.noConflict();
                                                        var revapi1;
                                                        tpj(document).ready(function() {
                                                            if(tpj("#rev_slider_1_1").revolution == undefined){
                                                                revslider_showDoubleJqueryError("#rev_slider_1_1");
                                                            }else{
                                                                revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                                                    sliderType:"standard",
                                                                    jsFileLocation:"//woodworker.thememove.com/wp-content/plugins/revslider/public/assets/js/",
                                                                    sliderLayout:"auto",
                                                                    dottedOverlay:"none",
                                                                    delay:6000,
                                                                    navigation: {
                                                                        keyboardNavigation:"off",
                                                                        keyboard_direction: "horizontal",
                                                                        mouseScrollNavigation:"off",
                                                                        mouseScrollReverse:"default",
                                                                        onHoverStop:"on",
                                                                        touch:{
                                                                            touchenabled:"on",
                                                                            swipe_threshold: 75,
                                                                            swipe_min_touches: 1,
                                                                            swipe_direction: "horizontal",
                                                                            drag_block_vertical: false
                                                                        }
                                                                        ,
                                                                        arrows: {
                                                                            style:"hebe",
                                                                            enable:true,
                                                                            hide_onmobile:true,
                                                                            hide_under:768,
                                                                            hide_onleave:false,
                                                                            tmp:'<div class="tp-title-wrap">	<span class="tp-arr-titleholder">{{title}}</span>    <span class="tp-arr-imgholder"></span> </div>',
                                                                            left: {
                                                                                h_align:"left",
                                                                                v_align:"center",
                                                                                h_offset:20,
                                                                                v_offset:0
                                                                            },
                                                                            right: {
                                                                                h_align:"right",
                                                                                v_align:"center",
                                                                                h_offset:20,
                                                                                v_offset:0
                                                                            }
                                                                        }
                                                                    },
                                                                    responsiveLevels:[1240,1240,1240,480],
                                                                    visibilityLevels:[1240,1240,1240,480],
                                                                    gridwidth:[1920,1024,778,480],
                                                                    gridheight:[680,768,960,720],
                                                                    lazyType:"none",
                                                                    parallax: {
                                                                        type:"mouse",
                                                                        origo:"enterpoint",
                                                                        speed:400,
                                                                        levels:[5,10,15,20,25,30,35,40,45,50,47,48,49,50,51,55],
                                                                        type:"mouse",
                                                                        disable_onmobile:"on"
                                                                    },
                                                                    shadow:0,
                                                                    spinner:"spinner2",
                                                                    stopLoop:"off",
                                                                    stopAfterLoops:-1,
                                                                    stopAtSlide:-1,
                                                                    shuffle:"off",
                                                                    autoHeight:"off",
                                                                    hideThumbsOnMobile:"off",
                                                                    hideSliderAtLimit:0,
                                                                    hideCaptionAtLimit:0,
                                                                    hideAllCaptionAtLilmit:0,
                                                                    debugMode:false,
                                                                    fallbacks: {
                                                                        simplifyAll:"off",
                                                                        nextSlideOnWindowFocus:"off",
                                                                        disableFocusListener:false,
                                                                    }
                                                                });
                                                            }
                                                        });	/*ready*/
                                                    </script>
                                                    <script>
                                                        var htmlDivCss = ' #rev_slider_1_1_wrapper .tp-loader.spinner2{ background-color: #FFFFFF !important; } ';
                                                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                        if(htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        }
                                                        else{
                                                            var htmlDiv = document.createElement('div');
                                                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                    <script>
                                                        var htmlDivCss = unescape(".hebe.tparrows%20%7B%0A%20%20cursor%3Apointer%3B%0A%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%20min-width%3A70px%3B%0A%20%20min-height%3A70px%3B%0A%20%20position%3Aabsolute%3B%0A%20%20display%3Ablock%3B%0A%20%20z-index%3A100%3B%0A%7D%0A.hebe.tparrows%3Ahover%20%7B%0A%7D%0A.hebe.tparrows%3Abefore%20%7B%0A%20%20font-family%3A%20%22revicons%22%3B%0A%20%20font-size%3A30px%3B%0A%20%20color%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20display%3Ablock%3B%0A%20%20line-height%3A%2070px%3B%0A%20%20text-align%3A%20center%3B%0A%20%20-webkit-transition%3A%20color%200.3s%3B%0A%20%20-moz-transition%3A%20color%200.3s%3B%0A%20%20transition%3A%20color%200.3s%3B%0A%20%20z-index%3A2%3B%0A%20%20position%3Arelative%3B%0A%20%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%20min-width%3A70px%3B%0A%20%20%20%20min-height%3A70px%3B%0A%7D%0A.hebe.tparrows.tp-leftarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hebe.tparrows.tp-rightarrow%3Abefore%20%7B%0A%20%20content%3A%20%22%5Ce825%22%3B%0A%7D%0A.hebe.tparrows%3Ahover%3Abefore%20%7B%0A%20color%3A%23000%3B%0A%20%20%20%20%20%20%7D%0A.tp-title-wrap%20%7B%20%0A%20%20position%3Aabsolute%3B%0A%20%20z-index%3A0%3B%0A%20%20display%3Ainline-block%3B%0A%20%20background%3A%23000%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%20min-height%3A60px%3B%0A%20%20line-height%3A60px%3B%0A%20%20top%3A-10px%3B%0A%20%20margin-left%3A0px%3B%0A%20%20-webkit-transition%3A%20-webkit-transform%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%3B%0A%20%20transform%3Ascalex%280%29%3B%20%20%0A%20%20-webkit-transform%3Ascalex%280%29%3B%20%20%0A%20%20transform-origin%3A0%25%2050%25%3B%20%0A%20%20%20-webkit-transform-origin%3A0%25%2050%25%3B%0A%7D%0A%20.hebe.tp-rightarrow%20.tp-title-wrap%20%7B%20%0A%20%20%20right%3A0px%3B%0A%20%20%20-webkit-transform-origin%3A100%25%2050%25%3B%0A%20%7D%0A.hebe.tparrows%3Ahover%20.tp-title-wrap%20%7B%0A%20%20transform%3Ascalex%281%29%3B%0A%20%20-webkit-transform%3Ascalex%281%29%3B%0A%7D%0A.hebe%20.tp-arr-titleholder%20%7B%0A%20%20position%3Arelative%3B%0A%20%20text-transform%3Auppercase%3B%0A%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20font-weight%3A600%3B%0A%20%20font-size%3A12px%3B%0A%20%20line-height%3A90px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20padding%3A0px%2020px%200px%2090px%3B%0A%7D%0A%0A.hebe.tp-rightarrow%20.tp-arr-titleholder%20%7B%0A%20%20%20margin-left%3A0px%3B%20%0A%20%20%20padding%3A0px%2090px%200px%2020px%3B%0A%20%7D%0A%0A.hebe.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20transition-delay%3A%200.1s%3B%0A%20%20opacity%3A1%3B%0A%7D%0A%0A.hebe%20.tp-arr-imgholder%7B%0A%20%20%20%20%20%20width%3A90px%3B%0A%20%20%20%20%20%20height%3A90px%3B%0A%20%20%20%20%20%20position%3Aabsolute%3B%0A%20%20%20%20%20%20left%3A100%25%3B%0A%20%20%20%20%20%20display%3Ablock%3B%0A%20%20%20%20%20%20background-size%3Acover%3B%0A%20%20%20%20%20%20background-position%3Acenter%20center%3B%0A%20%20%09%20top%3A0px%3B%20right%3A-90px%3B%0A%20%20%20%20%7D%0A.hebe.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20%20%20%20%20right%3Aauto%3Bleft%3A-90px%3B%0A%20%20%20%20%20%20%7D%0A");
                                                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                                        if(htmlDiv) {
                                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                        }
                                                        else{
                                                            var htmlDiv = document.createElement('div');
                                                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                                        }
                                                    </script>
                                                </div><!-- END REVOLUTION SLIDER --></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>