<?php include('header.php');?>
<?php include('primari.php');?>
<?php include('slider.php');?>
<div class="vc_row wpb_row vc_row-fluid vc_custom_1438160174654">
	<div class="wpb_column vc_column_container vc_col-sm-7">
		<div class="vc_column-inner vc_custom_1438933428760">
			<?php print_r($oldal->tartalom);?>
		</div>
	</div>
	<div class="tm_quick_quote wpb_column vc_column_container vc_col-sm-5 vc_col-has-fill">
		<div class="vc_column-inner vc_custom_1438928241336">
			<div class="wpb_wrapper">
				<div class="vc_custom_heading vc_custom_1438929246138" >
					<h2 style="font-size: 24px;color: #ffffff;line-height: 1;text-align: center" >Küldjön üzenetet</h2>
				</div>
				<div role="form" class="wpcf7" id="wpcf7-f162-p4-o1" lang="en-US" dir="ltr">
					<div class="screen-reader-response"></div>
						<form action="oldal/sendmail" method="post" class="wpcf7-form" novalidate="novalidate" style="padding-bottom:27px;">
							<div><span class="wpcf7-form-control-wrap your-name"><input type="text" name="senderName" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Név" /></span></div>
							<div><span class="wpcf7-form-control-wrap your-email"><input type="text" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" /></span></div>
							<div><span class="wpcf7-form-control-wrap your-email"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Tárgy" /></span></div>
							<div><span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Üzenet"></textarea></span></div>
							<div><input type="submit" value="Küldés" class="wpcf7-form-control wpcf7-submit" /></div>
							<div class="wpcf7-response-output wpcf7-display-none"></div>
						</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5" data-vc-parallax-image="http://woodworker.thememove.com/wp-content/uploads/2015/07/parallax_bg_01.jpg" class="vc_row wpb_row vc_row-fluid tm_services vc_custom_1460530968154 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460530687529"><div class="wpb_wrapper"><div class="vc_custom_heading style1 vc_custom_1438164805060" ><h2 style="font-size: 30px;color: #ffffff;text-align: left" >SZOLGÁLTATÁSAINK</h2></div><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_left-to-right vc_custom_1438917577098  icon">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="65" height="65" src="wp-content/uploads/2015/07/h1.png" class="vc_single_image-img attachment-full" alt="h1" /></div>
		</figure>
	</div>
<div class="vc_custom_heading vc_custom_1440746162167" ><h3 style="font-size: 15px;color: #333333;line-height: 1;text-align: center" >DESIGN KÉSZÍTÉS</h3></div><div class="vc_custom_heading vc_custom_1440746172090" ><div style="font-size: 14px;color: #aaaaaa;line-height: 1.8;text-align: center" >Plakátjaihoz designokat készítünk, és újrahasználunk.</div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left vc_custom_1438917588668  icon">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="65" height="65" src="wp-content/uploads/2015/07/h2.png" class="vc_single_image-img attachment-full" alt="h2" /></div>
		</figure>
	</div>
<div class="vc_custom_heading vc_custom_1440746184372" ><h3 style="font-size: 15px;color: #333333;line-height: 1;text-align: center" >TAPÉTA KÉSZÍTÉS</h3></div><div class="vc_custom_heading vc_custom_1440746197288" ><div style="font-size: 14px;color: #aaaaaa;line-height: 1.8;text-align: center" >Új tapéták elkészítése és forgalmazása</div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_left-to-right vc_custom_1438917597578  icon">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="65" height="65" src="wp-content/uploads/2015/07/h3.png" class="vc_single_image-img attachment-full" alt="h3" /></div>
		</figure>
	</div>
<div class="vc_custom_heading vc_custom_1440746209423" ><h3 style="font-size: 15px;color: #333333;line-height: 1;text-align: center" >POSZTER KÉSZÍTÉS</h3></div><div class="vc_custom_heading vc_custom_1440758791366" ><div style="font-size: 14px;color: #aaaaaa;line-height: 1.8;text-align: center" >Poszter designolás, és készítés is folyik nálunk </div></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_right-to-left vc_custom_1438917605011  icon">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="65" height="65" src="wp-content/uploads/2015/07/h4.png" class="vc_single_image-img attachment-full" alt="h4" /></div>
		</figure>
	</div>
<div class="vc_custom_heading vc_custom_1440746231301" ><h3 style="font-size: 15px;color: #333333;line-height: 1;text-align: center" >MÉRETRE SZABÁS</h3></div><div class="vc_custom_heading vc_custom_1440746242054" ><div style="font-size: 14px;color: #aaaaaa;line-height: 1.8;text-align: center" >Áruink a megadott méretre lesznek szabva. </div></div></div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid style1 tm-our-works vc_custom_1440405335248 vc_row-has-fill"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460530694290"><div class="wpb_wrapper"><div class="vc_custom_heading style1 vc_custom_1438164850920" ><h2 style="font-size: 30px;color: #ffffff;text-align: left" >PÁR MUNKÁNK</h2></div>
<div class="vc_btn3-container vc_btn3-inline"><a href="bemutatoterem"><button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-square vc_btn3-style-modern vc_btn3-color-grey">TOVÁBB A BEMUTATÓTEREMBE</button></a></div>
</div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid style1 vc_custom_1438921094634 vc_row-has-fill vc_row-no-padding"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460530703133"><div class="wpb_wrapper"><style type="text/css">a.eg-henryharrison-element-1,a.eg-henryharrison-element-2{-webkit-transition:all .4s linear;   -moz-transition:all .4s linear;   -o-transition:all .4s linear;   -ms-transition:all .4s linear;   transition:all .4s linear}.eg-jimmy-carter-element-11 i:before{margin-left:0px; margin-right:0px}.eg-harding-element-17{letter-spacing:1px}.eg-harding-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-harding-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-ulysses-s-grant-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-ulysses-s-grant-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-richard-nixon-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-richard-nixon-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-herbert-hoover-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-herbert-hoover-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-lyndon-johnson-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-lyndon-johnson-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.esg-overlay.eg-ronald-reagan-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-georgebush-wrapper .esg-entry-cover{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-jefferson-wrapper{-webkit-border-radius:5px !important; -moz-border-radius:5px !important; border-radius:5px !important; -webkit-mask-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important}.eg-monroe-element-1{text-shadow:0px 1px 3px rgba(0,0,0,0.1)}.eg-lyndon-johnson-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-media img{-webkit-transition:0.4s ease-in-out;  -moz-transition:0.4s ease-in-out;  -o-transition:0.4s ease-in-out;  transition:0.4s ease-in-out;  filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-wilbert-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-phillie-element-3:after{content:" ";width:0px;height:0px;border-style:solid;border-width:5px 5px 0 5px;border-color:#000 transparent transparent transparent;left:50%;margin-left:-5px; bottom:-5px; position:absolute}.eg-howardtaft-wrapper .esg-entry-media img,.eg-howardtaft-wrapper .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-howardtaft-wrapper:hover .esg-entry-media img,.eg-howardtaft-wrapper:hover .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.myportfolio-container .added_to_cart.wc-forward{font-family:"Open Sans"; font-size:13px; color:#fff; margin-top:10px}.esgbox-title.esgbox-title-outside-wrap{font-size:15px; font-weight:700; text-align:center}.esgbox-title.esgbox-title-inside-wrap{padding-bottom:10px; font-size:15px; font-weight:700; text-align:center}</style>
<!-- CACHE FOUND FOR: 3 --><style type="text/css">.tm-skin .navigationbuttons,.tm-skin .esg-pagination,.tm-skin .esg-filters{text-transform:uppercase;  text-align:center}.tm-skin .esg-filterbutton:before{content:'';  position:absolute;  left:0;  top:0;  width:0;  height:0;  border-style:solid;  border-width:20px 20px 0 0;  border-color:transparent transparent transparent transparent}.tm-skin .esg-filterbutton,.tm-skin .esg-navigationbutton,.tm-skin .esg-sortbutton,.tm-skin .esg-cartbutton{color:#fff;  cursor:pointer;  position:relative;  z-index:2;  padding:15px 25px;  font-size:14px;  font-weight:700;  display:inline-block;  margin-bottom:5px;  margin-right:10px;  text-transform:uppercase;  border:2px solid #C69453}.tm-skin .esg-navigationbutton{padding:2px 12px}.tm-skin .esg-navigationbutton *{color:#000}.tm-skin .esg-pagination-button:last-child{margin-right:0}.tm-skin .esg-sortbutton-wrapper,.tm-skin .esg-cartbutton-wrapper{display:inline-block}.tm-skin .esg-sortbutton-order,.tm-skin .esg-cartbutton-order{display:inline-block;  vertical-align:top;  border:none;  width:40px;  line-height:40px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px;  font-size:12px;  font-weight:700;  color:#999;  cursor:pointer;  background:#fff;  margin-left:5px}.tm-skin .esg-cartbutton{color:#fff;  cursor:default !important}.tm-skin .esg-cartbutton .esgicon-basket{color:#fff;  font-size:15px;  line-height:15px;  margin-right:10px}.tm-skin .esg-cartbutton-wrapper{cursor:default !important}.tm-skin .esg-sortbutton,.tm-skin .esg-cartbutton{display:inline-block;  position:relative;  cursor:pointer;  margin-right:0px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px}.tm-skin .esg-navigationbutton:hover,.tm-skin .esg-filterbutton:hover,.tm-skin .esg-sortbutton:hover,.tm-skin .esg-sortbutton-order:hover,.tm-skin .esg-cartbutton-order:hover,.tm-skin .esg-filterbutton.selected{background:#C69453}.tm-skin .esg-navigationbutton:hover *{color:#333}.tm-skin .esg-sortbutton-order.tp-desc:hover{color:#333}.tm-skin .esg-filter-checked{padding:1px 3px;  color:#cbcbcb;  background:#cbcbcb;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle;  display:none}.tm-skin .esg-filterbutton.selected .esg-filter-checked,.tm-skin .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;  color:#fff;  background:#000;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}</style>
<!-- ESSENTIAL GRID SKIN CSS -->
<style type="text/css">.eg-tm-projects-01-element-3{font-size:18px; line-height:25px; color:#ffffff; font-weight:700; padding:25px 25px 23px 25px ; border-radius:0px 0px 0px 0px ; background-color:rgba(198,148,83,1.00); z-index:2 !important; display:block}.eg-tm-projects-01-element-0{font-size:20px !important; line-height:20px !important; color:#ffffff !important; font-weight:400 !important; padding:17px 17px 17px 17px !important; border-radius:60px 60px 60px 60px !important; background-color:rgba(255,255,255,0) !important; z-index:2 !important; display:block; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}</style>
<style type="text/css">.eg-tm-projects-01-element-3:hover{font-size:18px; line-height:25px; color:#3e3024; font-weight:700; border-radius:0px 0px 0px 0px ; background-color:rgba(198,148,83,1.00)}.eg-tm-projects-01-element-0:hover{font-size:20px !important; line-height:20px !important; color:#ffffff !important; font-weight:400 !important; border-radius:60px 60px 60px 60px !important; background-color:rgba(0,0,0,0.50) !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}</style>
<style type="text/css">.eg-tm-projects-01-element-3-a{display:block; text-align:left; clear:both; margin:0px 0px 0px 0px ; position:relative}</style>
<style type="text/css">.eg-tm-projects-01-element-0-a{display:inline-block !important; float:none !important; clear:none !important; margin:0px 0px 0px 0px !important; position:relative !important}</style>
<style type="text/css">.eg-tm-projects-01-container{background-color:rgba(0,0,0,0.65)}</style>
<style type="text/css">.eg-tm-projects-01-content{background-color:#ffffff; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:double; text-align:left}</style>
<style type="text/css">.esg-grid .mainul li.eg-tm-projects-01-wrapper{background-color:#c69453; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:none}</style>
<!-- ESSENTIAL GRID END SKIN CSS -->

<!-- THE ESSENTIAL GRID 2.0.9.1 -->

<!-- GRID WRAPPER FOR CONTAINER SIZING - HERE YOU CAN SET THE CONTAINER SIZE AND CONTAINER SKIN -->
<article class="myportfolio-container tm-skin" id="esg-grid-3-1-wrap">

    <!-- THE GRID ITSELF WITH FILTERS, PAGINATION, SORTING ETC... -->
    <div id="esg-grid-3-1" class="esg-grid" style="background-color: transparent;padding: 0px 0px 0px 0px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">
<!-- ############################ -->
<!-- THE GRID ITSELF WITH ENTRIES -->
<!-- ############################ -->
<ul>
<?php foreach($galeria->result() as $row){?>
	<li class="filterall filter-art filter-design filter-funiture filter-interior filter-work eg-tm-projects-01-wrapper eg-post-id-143" data-date="1438920685">
		<!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
		<div class="esg-media-cover-wrapper">
				<!-- THE MEDIA OF THE ENTRY -->
	<div class="esg-entry-media esg-shifttotop" data-delay="0.17"><img src="assets/uploads/galeria/<?php echo $row->file?>" alt=""></div>

				<!-- THE CONTENT OF THE ENTRY -->
				<div class="esg-entry-cover">

					<!-- THE COLORED OVERLAY -->
					<div class="esg-overlay esg-fade eg-tm-projects-01-container" data-delay="0"></div>
			   </div><!-- END OF THE CONTENT IN THE ENTRY -->
	   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

	</li><!-- END OF PORTFOLIO ITEM -->
<?php }?>
</ul>
<!-- ############################ -->
<!--      END OF THE GRID         -->
<!-- ############################ -->
    </div><!-- END OF THE GRID -->

</article>
<!-- END OF THE GRID WRAPPER -->

<div class="clear"></div>
<script type="text/javascript">
function eggbfc(winw,resultoption) {
	var lasttop = winw,
	lastbottom = 0,
	smallest =9999,
	largest = 0,
	samount = 0,
	lamoung = 0,
	lastamount = 0,
	resultid = 0,
	resultidb = 0,
	responsiveEntries = [
						{ width:1400,amount:5},
						{ width:1170,amount:4},
						{ width:1024,amount:3},
						{ width:960,amount:3},
						{ width:778,amount:3},
						{ width:640,amount:3},
						{ width:480,amount:1}
						];
	if (responsiveEntries!=undefined && responsiveEntries.length>0)
		jQuery.each(responsiveEntries, function(index,obj) {
			var curw = obj.width != undefined ? obj.width : 0,
				cura = obj.amount != undefined ? obj.amount : 0;
			if (smallest>curw) {
				smallest = curw;
				samount = cura;
				resultidb = index;
			}
			if (largest<curw) {
				largest = curw;
				lamount = cura;
			}
			if (curw>lastbottom && curw<=lasttop) {
				lastbottom = curw;
				lastamount = cura;
				resultid = index;
			}
		})
		if (smallest>winw) {
			lastamount = samount;
			resultid = resultidb;
		}
		var obj = new Object;
		obj.index = resultid;
		obj.column = lastamount;
		if (resultoption=="id")
			return obj;
		else
			return lastamount;
	}
if ("even"=="even") {
	var coh=0,
		container = jQuery("#esg-grid-3-1");
	var	cwidth = container.width(),
		ar = "4:3",
		gbfc = eggbfc(jQuery(window).width(),"id"),
	row = 1;
ar = ar.split(":");
aratio=parseInt(ar[0],0) / parseInt(ar[1],0);
coh = cwidth / aratio;
coh = coh/gbfc.column*row;
	var ul = container.find("ul").first();
	ul.css({display:"block",height:coh+"px"});
}
var essapi_3;
jQuery(document).ready(function() {
	essapi_3 = jQuery("#esg-grid-3-1").tpessential({
        gridID:3,
        layout:"even",
        forceFullWidth:"off",
        lazyLoad:"off",
        row:1,
        loadMoreAjaxToken:"399dad924b",
        loadMoreAjaxUrl:"http://woodworker.thememove.com/wp-admin/admin-ajax.php",
        loadMoreAjaxAction:"Essential_Grid_Front_request_ajax",
        ajaxContentTarget:"ess-grid-ajax-container-",
        ajaxScrollToOffset:"0",
        ajaxCloseButton:"off",
        ajaxContentSliding:"on",
        ajaxScrollToOnLoad:"on",
        ajaxNavButton:"off",
        ajaxCloseType:"type1",
        ajaxCloseInner:"false",
        ajaxCloseStyle:"light",
        ajaxClosePosition:"tr",
        space:0,
        pageAnimation:"fade",
        paginationScrollToTop:"off",
        spinner:"spinner3",
        spinnerColor:"#FFFFFF",
        evenGridMasonrySkinPusher:"off",
        lightBoxMode:"single",
        animSpeed:1000,
        delayBasic:1,
        mainhoverdelay:1,
        filterType:"single",
        showDropFilter:"hover",
        filterGroupClass:"esg-fgc-3",
        googleFonts:['Open+Sans:300,400,600,700,800','Raleway:100,200,300,400,500,600,700,800,900','Droid+Serif:400,700'],
        aspectratio:"4:3",
        responsiveEntries: [
						{ width:1400,amount:5},
						{ width:1170,amount:4},
						{ width:1024,amount:3},
						{ width:960,amount:3},
						{ width:778,amount:3},
						{ width:640,amount:3},
						{ width:480,amount:1}
						]
	});

	try{
	jQuery("#esg-grid-3-1 .esgbox").esgbox({
		padding : [0,0,0,0],
      afterLoad:function() { 
 		if (this.element.hasClass("esgboxhtml5")) {
		   var mp = this.element.data("mp4"),
		      ogv = this.element.data("ogv"),
		      webm = this.element.data("webm");
         this.content ='<div style="width:100%;height:100%;"><video autoplay="true" loop="" class="rowbgimage" poster="" width="100%" height="auto"><source src="'+mp+'" type="video/mp4"><source src="'+webm+'" type="video/webm"><source src="'+ogv+'" type="video/ogg"></video></div>';	
		   var riint = setInterval(function() {jQuery(window).trigger("resize");},100); setTimeout(function() {clearInterval(riint);},2500);
		   };
		 },
		beforeShow : function () { 
			this.title = jQuery(this.element).attr('lgtitle');
			if (this.title) {
				this.title="";
   		this.title =  '<div style="padding:0px 0px 0px 0px">'+this.title+'</div>';
			}
		},
		afterShow : function() {
		},
		openEffect : 'fade',
		closeEffect : 'fade',
		nextEffect : 'fade',
		prevEffect : 'fade',
		openSpeed : 'normal',
		closeSpeed : 'normal',
		nextSpeed : 'normal',
		prevSpeed : 'normal',
		helpers : {
			media : {},
		    title : {
				type:""
			}
		}
});

 } catch (e) {}

});
</script>
</div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>
						<!-- .entry-content -->
					</article><!-- #post-## -->
												</div>
		</div>
			</div>
</div>

</div><!-- #content -->
    <?php include('footer.php');?>