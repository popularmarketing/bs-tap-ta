<?php include('header.php');?>
<?php include('primari.php');?>

		<div id="content" class="site-content">
	<div class="big-title" style="background-image: url('../data/images/page_bg.jpg')">
		<div class="container">
			<div class="row middle">
				<div class="col-md-8">
					<h1 class="entry-title" itemprop="headline">Bemutatóterem</h1>					<h3>Exkluzív belvárosi bemutatótermünkben több mint 10 000 féle termékből választhatnak vásárlóink. </h3>				</div>
				<div class="col-md-4 end">
											<div class="breadcrumb">
							<div class="container">
								<ul class="tm_bread_crumb">
	<li class="level-1 top"><a href="fooldal">Nyitólap</a></li>
	<li class="level-2 sub tail current">Bemutatóterem</li>
</ul>
							</div>
						</div>
									</div>
			</div>
		</div>
	</div>
<div class="container">
	<div class="row">
											<div class="col-md-12">
			<div class="content">
									<article id="post-6">
						<div class="entry-content">
							<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid style1 vc_custom_1439007371668"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460533348633"><div class="wpb_wrapper">

</div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid style1 vc_custom_1439007402821 vc_row-no-padding"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460533326481"><div class="wpb_wrapper"><style type="text/css">a.eg-henryharrison-element-1,a.eg-henryharrison-element-2{-webkit-transition:all .4s linear;   -moz-transition:all .4s linear;   -o-transition:all .4s linear;   -ms-transition:all .4s linear;   transition:all .4s linear}.eg-jimmy-carter-element-11 i:before{margin-left:0px; margin-right:0px}.eg-harding-element-17{letter-spacing:1px}.eg-harding-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-harding-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-ulysses-s-grant-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-ulysses-s-grant-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-richard-nixon-wrapper .esg-entry-media{overflow:hidden; box-sizing:border-box;   -webkit-box-sizing:border-box;   -moz-box-sizing:border-box;   padding:30px 30px 0px 30px}.eg-richard-nixon-wrapper .esg-entry-media img{overflow:hidden; border-radius:50%;   -webkit-border-radius:50%;   -moz-border-radius:50%}.eg-herbert-hoover-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-herbert-hoover-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-lyndon-johnson-wrapper .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-lyndon-johnson-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.esg-overlay.eg-ronald-reagan-container{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-georgebush-wrapper .esg-entry-cover{background:-moz-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-webkit-gradient(linear,left top,left bottom,color-stop(50%,rgba(0,0,0,0)),color-stop(99%,rgba(0,0,0,0.83)),color-stop(100%,rgba(0,0,0,0.85))); background:-webkit-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-o-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:-ms-linear-gradient(top,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); background:linear-gradient(to bottom,rgba(0,0,0,0) 50%,rgba(0,0,0,0.83) 99%,rgba(0,0,0,0.85) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000',endColorstr='#d9000000',GradientType=0 )}.eg-jefferson-wrapper{-webkit-border-radius:5px !important; -moz-border-radius:5px !important; border-radius:5px !important; -webkit-mask-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important}.eg-monroe-element-1{text-shadow:0px 1px 3px rgba(0,0,0,0.1)}.eg-lyndon-johnson-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-cover{background:-moz-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-webkit-gradient(radial,center center,0px,center center,100%,color-stop(0%,rgba(0,0,0,0.35)),color-stop(96%,rgba(18,18,18,0)),color-stop(100%,rgba(19,19,19,0))); background:-webkit-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-o-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:-ms-radial-gradient(center,ellipse cover,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); background:radial-gradient(ellipse at center,rgba(0,0,0,0.35) 0%,rgba(18,18,18,0) 96%,rgba(19,19,19,0) 100%); filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#59000000',endColorstr='#00131313',GradientType=1 )}.eg-wilbert-wrapper .esg-entry-media img{-webkit-transition:0.4s ease-in-out;  -moz-transition:0.4s ease-in-out;  -o-transition:0.4s ease-in-out;  transition:0.4s ease-in-out;  filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.eg-wilbert-wrapper:hover .esg-entry-media img{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-phillie-element-3:after{content:" ";width:0px;height:0px;border-style:solid;border-width:5px 5px 0 5px;border-color:#000 transparent transparent transparent;left:50%;margin-left:-5px; bottom:-5px; position:absolute}.eg-howardtaft-wrapper .esg-entry-media img,.eg-howardtaft-wrapper .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");  -webkit-filter:grayscale(0%)}.eg-howardtaft-wrapper:hover .esg-entry-media img,.eg-howardtaft-wrapper:hover .esg-media-poster{filter:url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");   filter:gray;   -webkit-filter:grayscale(100%)}.myportfolio-container .added_to_cart.wc-forward{font-family:"Open Sans"; font-size:13px; color:#fff; margin-top:10px}.esgbox-title.esgbox-title-outside-wrap{font-size:15px; font-weight:700; text-align:center}.esgbox-title.esgbox-title-inside-wrap{padding-bottom:10px; font-size:15px; font-weight:700; text-align:center}</style>
<!-- CACHE FOUND FOR: 8 --><style type="text/css">.tm-skin .navigationbuttons,.tm-skin .esg-pagination,.tm-skin .esg-filters{text-transform:uppercase;  text-align:center}.tm-skin .esg-filterbutton:before{content:'';  position:absolute;  left:0;  top:0;  width:0;  height:0;  border-style:solid;  border-width:20px 20px 0 0;  border-color:transparent transparent transparent transparent}.tm-skin .esg-filterbutton,.tm-skin .esg-navigationbutton,.tm-skin .esg-sortbutton,.tm-skin .esg-cartbutton{color:#fff;  cursor:pointer;  position:relative;  z-index:2;  padding:15px 25px;  font-size:14px;  font-weight:700;  display:inline-block;  margin-bottom:5px;  margin-right:10px;  text-transform:uppercase;  border:2px solid #C69453}.tm-skin .esg-navigationbutton{padding:2px 12px}.tm-skin .esg-navigationbutton *{color:#000}.tm-skin .esg-pagination-button:last-child{margin-right:0}.tm-skin .esg-sortbutton-wrapper,.tm-skin .esg-cartbutton-wrapper{display:inline-block}.tm-skin .esg-sortbutton-order,.tm-skin .esg-cartbutton-order{display:inline-block;  vertical-align:top;  border:none;  width:40px;  line-height:40px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px;  font-size:12px;  font-weight:700;  color:#999;  cursor:pointer;  background:#fff;  margin-left:5px}.tm-skin .esg-cartbutton{color:#fff;  cursor:default !important}.tm-skin .esg-cartbutton .esgicon-basket{color:#fff;  font-size:15px;  line-height:15px;  margin-right:10px}.tm-skin .esg-cartbutton-wrapper{cursor:default !important}.tm-skin .esg-sortbutton,.tm-skin .esg-cartbutton{display:inline-block;  position:relative;  cursor:pointer;  margin-right:0px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px}.tm-skin .esg-navigationbutton:hover,.tm-skin .esg-filterbutton:hover,.tm-skin .esg-sortbutton:hover,.tm-skin .esg-sortbutton-order:hover,.tm-skin .esg-cartbutton-order:hover,.tm-skin .esg-filterbutton.selected{background:#C69453}.tm-skin .esg-navigationbutton:hover *{color:#333}.tm-skin .esg-sortbutton-order.tp-desc:hover{color:#333}.tm-skin .esg-filter-checked{padding:1px 3px;  color:#cbcbcb;  background:#cbcbcb;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle;  display:none}.tm-skin .esg-filterbutton.selected .esg-filter-checked,.tm-skin .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;  color:#fff;  background:#000;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}</style>
<style type="text/css">.tm-skin-2 .navigationbuttons,.tm-skin-2 .esg-pagination,.tm-skin-2 .esg-filters{text-transform:uppercase;  text-align:center}.tm-skin-2 .esg-filterbutton:before{content:'';  position:absolute;  left:0;  top:0;  width:0;  height:0;  border-style:solid;  border-width:20px 20px 0 0;  border-color:transparent transparent transparent transparent}.tm-skin-2 .esg-filterbutton,.tm-skin-2 .esg-navigationbutton,.tm-skin-2 .esg-sortbutton,.tm-skin-2 .esg-cartbutton{color:#594431;  cursor:pointer;  position:relative;  z-index:2;  padding:15px 25px;  font-size:14px;  font-weight:700;  display:inline-block;  margin-bottom:5px;  margin-right:10px;  text-transform:uppercase;  border:2px solid #C69453}.tm-skin-2 .esg-navigationbutton{padding:10px 12px;  background-color:#fff;  position:absolute;  left:-70px}.tm-skin-2 .esg-right{left:auto;  right:-70px}.tm-skin-2 .esg-navigationbutton *{color:#000}.tm-skin-2 .esg-pagination-button:last-child{margin-right:0}.tm-skin-2 .esg-sortbutton-wrapper,.tm-skin-2 .esg-cartbutton-wrapper{display:inline-block}.tm-skin-2 .esg-sortbutton-order,.tm-skin-2 .esg-cartbutton-order{display:inline-block;  vertical-align:top;  border:none;  width:40px;  line-height:40px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px;  font-size:12px;  font-weight:700;  color:#999;  cursor:pointer;  background:#594431;  margin-left:5px}.tm-skin-2 .esg-cartbutton{color:#594431;  cursor:default !important}.tm-skin-2 .esg-cartbutton .esgicon-basket{color:#594431;  font-size:15px;  line-height:15px;  margin-right:10px}.tm-skin-2 .esg-cartbutton-wrapper{cursor:default !important}.tm-skin-2 .esg-sortbutton,.tm-skin-2 .esg-cartbutton{display:inline-block;  position:relative;  cursor:pointer;  margin-right:0px;  border-radius:5px;  -moz-border-radius:5px;  -webkit-border-radius:5px}.tm-skin-2 .esg-navigationbutton:hover,.tm-skin-2 .esg-filterbutton:hover,.tm-skin-2 .esg-sortbutton:hover,.tm-skin-2 .esg-sortbutton-order:hover,.tm-skin-2 .esg-cartbutton-order:hover,.tm-skin-2 .esg-filterbutton.selected{background:#C69453}.tm-skin-2 .esg-navigationbutton:hover *{color:#333}.tm-skin-2 .esg-sortbutton-order.tp-desc:hover{color:#333}.tm-skin-2 .esg-filter-checked{padding:1px 3px;  color:#cbcbcb;  background:#cbcbcb;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle;  display:none}.tm-skin-2 .esg-filterbutton.selected .esg-filter-checked,.tm-skin-2 .esg-filterbutton:hover .esg-filter-checked{padding:1px 3px 1px 3px;  color:#594431;  background:#000;  margin-left:7px;  font-size:9px;  font-weight:300;  line-height:9px;  vertical-align:middle}</style>
<!-- ESSENTIAL GRID SKIN CSS -->
<style type="text/css">.eg-tm-projects-01-element-3{font-size:18px; line-height:25px; color:#ffffff; font-weight:700; padding:25px 25px 23px 25px ; border-radius:0px 0px 0px 0px ; background-color:rgba(198,148,83,1.00); z-index:2 !important; display:block}.eg-tm-projects-01-element-0{font-size:20px !important; line-height:20px !important; color:#ffffff !important; font-weight:400 !important; padding:17px 17px 17px 17px !important; border-radius:60px 60px 60px 60px !important; background-color:rgba(255,255,255,0) !important; z-index:2 !important; display:block; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}</style>
<style type="text/css">.eg-tm-projects-01-element-3:hover{font-size:18px; line-height:25px; color:#3e3024; font-weight:700; border-radius:0px 0px 0px 0px ; background-color:rgba(198,148,83,1.00)}.eg-tm-projects-01-element-0:hover{font-size:20px !important; line-height:20px !important; color:#ffffff !important; font-weight:400 !important; border-radius:60px 60px 60px 60px !important; background-color:rgba(0,0,0,0.50) !important; border-top-width:0px !important; border-right-width:0px !important; border-bottom-width:0px !important; border-left-width:0px !important; border-color:#ffffff !important; border-style:solid !important}</style>
<style type="text/css">.eg-tm-projects-01-element-3-a{display:block; text-align:left; clear:both; margin:0px 0px 0px 0px ; position:relative}</style>
<style type="text/css">.eg-tm-projects-01-element-0-a{display:inline-block !important; float:none !important; clear:none !important; margin:0px 0px 0px 0px !important; position:relative !important}</style>
<style type="text/css">.eg-tm-projects-01-container{background-color:rgba(0,0,0,0.65)}</style>
<style type="text/css">.eg-tm-projects-01-content{background-color:#ffffff; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:double; text-align:left}</style>
<style type="text/css">.esg-grid .mainul li.eg-tm-projects-01-wrapper{background-color:#c69453; padding:0px 0px 0px 0px; border-width:0px 0px 0px 0px; border-radius:0px 0px 0px 0px; border-color:transparent; border-style:none}</style>
<!-- ESSENTIAL GRID END SKIN CSS -->

<!-- THE ESSENTIAL GRID 2.0.9.1 -->

<!-- GRID WRAPPER FOR CONTAINER SIZING - HERE YOU CAN SET THE CONTAINER SIZE AND CONTAINER SKIN -->
<article class="myportfolio-container tm-skin" id="esg-grid-8-1-wrap">

    <!-- THE GRID ITSELF WITH FILTERS, PAGINATION, SORTING ETC... -->
    <div id="esg-grid-8-1" class="esg-grid" style="background-color: transparent;padding: 0px 0px 0px 0px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box;">
<!-- ############################ -->
<!-- THE GRID ITSELF WITH ENTRIES -->
<!-- ############################ -->
<ul>
<!-- PORTFOLIO ITEM 47 -->
<?php foreach($galeria->result() as $row){?>
<li class="filterall filter-art filter-design filter-funiture filter-interior filter-work eg-tm-projects-01-wrapper eg-post-id-143" data-date="1438920685">
    <!-- THE CONTAINER FOR THE MEDIA AND THE COVER EFFECTS -->
    <div class="esg-media-cover-wrapper">
            <!-- THE MEDIA OF THE ENTRY -->
<div class="esg-entry-media esg-shifttotop" data-delay="0.17"><img src="assets/uploads/galeria/<?php echo $row->file?>" alt=""></div>

            <!-- THE CONTENT OF THE ENTRY -->
            <div class="esg-entry-cover">

                <!-- THE COLORED OVERLAY -->
                <div class="esg-overlay esg-fade eg-tm-projects-01-container" data-delay="0"></div>

				<div class="esg-center eg-post-143 eg-tm-projects-01-element-0-a esg-falldown" data-delay="0.1"><a class="eg-tm-projects-01-element-0 eg-post-143 esgbox" href="assets/uploads/galeria/<?php echo $row->file?>" lgtitle="Interior Design"><i class="eg-icon-search"></i></a></div>
           </div><!-- END OF THE CONTENT IN THE ENTRY -->
   </div><!-- END OF THE CONTAINER FOR THE MEDIA AND COVER/HOVER EFFECTS -->

</li><!-- END OF PORTFOLIO ITEM -->
<?php }?>
</ul>
<!-- ############################ -->
<!--      END OF THE GRID         -->
<!-- ############################ -->
 </div><!-- END OF THE GRID -->

</article>
<!-- END OF THE GRID WRAPPER -->

<div class="clear"></div>
<script type="text/javascript">
function eggbfc(winw,resultoption) {
	var lasttop = winw,
	lastbottom = 0,
	smallest =9999,
	largest = 0,
	samount = 0,
	lamoung = 0,
	lastamount = 0,
	resultid = 0,
	resultidb = 0,
	responsiveEntries = [
						{ width:1400,amount:5},
						{ width:1170,amount:4},
						{ width:1024,amount:3},
						{ width:960,amount:3},
						{ width:778,amount:3},
						{ width:640,amount:3},
						{ width:480,amount:1}
						];
	if (responsiveEntries!=undefined && responsiveEntries.length>0)
		jQuery.each(responsiveEntries, function(index,obj) {
			var curw = obj.width != undefined ? obj.width : 0,
				cura = obj.amount != undefined ? obj.amount : 0;
			if (smallest>curw) {
				smallest = curw;
				samount = cura;
				resultidb = index;
			}
			if (largest<curw) {
				largest = curw;
				lamount = cura;
			}
			if (curw>lastbottom && curw<=lasttop) {
				lastbottom = curw;
				lastamount = cura;
				resultid = index;
			}
		})
		if (smallest>winw) {
			lastamount = samount;
			resultid = resultidb;
		}
		var obj = new Object;
		obj.index = resultid;
		obj.column = lastamount;
		if (resultoption=="id")
			return obj;
		else
			return lastamount;
	}
if ("even"=="even") {
	var coh=0,
		container = jQuery("#esg-grid-8-1");
	var	cwidth = container.width(),
		ar = "4:3",
		gbfc = eggbfc(jQuery(window).width(),"id"),
	row = 2;
ar = ar.split(":");
aratio=parseInt(ar[0],0) / parseInt(ar[1],0);
coh = cwidth / aratio;
coh = coh/gbfc.column*row;
	var ul = container.find("ul").first();
	ul.css({display:"block",height:coh+"px"});
}
var essapi_8;
jQuery(document).ready(function() {
	essapi_8 = jQuery("#esg-grid-8-1").tpessential({
        gridID:8,
        layout:"even",
        forceFullWidth:"off",
        lazyLoad:"off",
        row:2,
        loadMoreAjaxToken:"399dad924b",
        loadMoreAjaxUrl:"http://woodworker.thememove.com/wp-admin/admin-ajax.php",
        loadMoreAjaxAction:"Essential_Grid_Front_request_ajax",
        ajaxContentTarget:"ess-grid-ajax-container-",
        ajaxScrollToOffset:"0",
        ajaxCloseButton:"off",
        ajaxContentSliding:"on",
        ajaxScrollToOnLoad:"on",
        ajaxNavButton:"off",
        ajaxCloseType:"type1",
        ajaxCloseInner:"false",
        ajaxCloseStyle:"light",
        ajaxClosePosition:"tr",
        space:0,
        pageAnimation:"fade",
        paginationScrollToTop:"off",
        spinner:"spinner3",
        spinnerColor:"#FFFFFF",
        evenGridMasonrySkinPusher:"off",
        lightBoxMode:"single",
        animSpeed:1000,
        delayBasic:1,
        mainhoverdelay:1,
        filterType:"single",
        showDropFilter:"hover",
        filterGroupClass:"esg-fgc-8",
        googleFonts:['Open+Sans:300,400,600,700,800','Raleway:100,200,300,400,500,600,700,800,900','Droid+Serif:400,700'],
        aspectratio:"4:3",
        responsiveEntries: [
						{ width:1400,amount:5},
						{ width:1170,amount:4},
						{ width:1024,amount:3},
						{ width:960,amount:3},
						{ width:778,amount:3},
						{ width:640,amount:3},
						{ width:480,amount:1}
						]
	});

	try{
	jQuery("#esg-grid-8-1 .esgbox").esgbox({
		padding : [0,0,0,0],
      afterLoad:function() { 
 		if (this.element.hasClass("esgboxhtml5")) {
		   var mp = this.element.data("mp4"),
		      ogv = this.element.data("ogv"),
		      webm = this.element.data("webm");
         this.content ='<div style="width:100%;height:100%;"><video autoplay="true" loop="" class="rowbgimage" poster="" width="100%" height="auto"><source src="'+mp+'" type="video/mp4"><source src="'+webm+'" type="video/webm"><source src="'+ogv+'" type="video/ogg"></video></div>';	
		   var riint = setInterval(function() {jQuery(window).trigger("resize");},100); setTimeout(function() {clearInterval(riint);},2500);
		   };
		 },
		beforeShow : function () { 
			this.title = jQuery(this.element).attr('lgtitle');
			if (this.title) {
				this.title="";
   		this.title =  '<div style="padding:0px 0px 0px 0px">'+this.title+'</div>';
			}
		},
		afterShow : function() {
		},
		openEffect : 'fade',
		closeEffect : 'fade',
		nextEffect : 'fade',
		prevEffect : 'fade',
		openSpeed : 'normal',
		closeSpeed : 'normal',
		nextSpeed : 'normal',
		prevSpeed : 'normal',
		helpers : {
			media : {},
		    title : {
				type:""
			}
		}
});

 } catch (e) {}

});
</script>
</div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>
													</div>
						<!-- .entry-content -->
					</article><!-- #post-## -->
												</div>
		</div>
			</div>
</div>

</div><!-- #content -->
   <?php include('footer.php');?>