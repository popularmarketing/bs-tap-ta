<?php include('header.php');?>
<?php include('primari.php');?>
	<div id="content" class="site-content">
	<div class="big-title" style="background-image: url('../data/images/page_bg.jpg')">
		<div class="container">
			<div class="row middle">
				<div class="col-md-8">
					<h1 class="entry-title" itemprop="headline">Kapcsolat</h1>					<h3>Írjon nekünk üzenetet, amint tudunk, válaszolunk.</h3>
				</div>
				<div class="col-md-4 end">
					<div class="breadcrumb">
							<div class="container">
								<ul class="tm_bread_crumb">
									<li class="level-1 top"><a href="fooldal">Nyitólap</a></li>
									<li class="level-2 sub tail current">Kapcsolat</li>
								</ul>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<article id="post-65">
						<div class="entry-content">
							<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_custom_1438942178139 vc_row-no-padding"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460539801188"><div class="wpb_wrapper"><div id="map-canvas" class="thememove-gmaps"	data-address="47.532413,21.631803" data-height="480" data-width="100%" data-zoom_enable="" data-zoom="16" data-map_type="roadmap" data-map_style="default"></div>
											<script type="text/javascript">
												jQuery(document).ready(function ($) {

													var gmMapDiv = $("#map-canvas");

													(function ($) {

														if (gmMapDiv.length) {

															var gmMarkerAddress = gmMapDiv.attr("data-address");
															var gmHeight = gmMapDiv.attr("data-height");
															var gmWidth = gmMapDiv.attr("data-width");
															var gmZoomEnable = gmMapDiv.attr("data-zoom_enable");
															var gmZoom = gmMapDiv.attr("data-zoom");

															gmMapDiv.gmap3({
																action: "init",
																marker: {
																	address: gmMarkerAddress,
																	options: {
																									icon: "http://woodworker.thememove.com/wp-content/themes/tm-wood-worker/images/map-marker.png",
																								},
																						},
																map: {
																	options: {
																		zoom: parseInt(gmZoom),
																		zoomControl: true,
																		mapTypeId: google.maps.MapTypeId.ROADMAP,
																		mapTypeControl: false,
																		scaleControl: false,
																		scrollwheel: gmZoomEnable == 'enable' ? true : false,
																		streetViewControl: false,
																		draggable: true,
																								}
																}
															}).width(gmWidth).height(gmHeight);
														}
													})(jQuery);
												});
											</script>
										</div>
									</div>
								</div>
							</div>
							<div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid vc_custom_1438942231820"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner vc_custom_1460539806914"><div class="wpb_wrapper"><div class="vc_custom_heading style2 vc_custom_1438941801017" ><h2 style="font-size: 30px;color: #333333;text-align: left" >KÜLDJÖN ÜZENETET</h2></div><div role="form" class="wpcf7" id="wpcf7-f222-p65-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="oldal/sendmail2" method="post" class="wpcf7-form" novalidate="novalidate">
<div class="row">
<div class="col-md-3">
<div><span class="wpcf7-form-control-wrap your-name"><input type="text" name="senderName" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Név" /></span></div>
<div><span class="wpcf7-form-control-wrap your-address"><input type="text" name="address" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Cím" /></span></div></div>
<div class="col-md-3">
<div><span class="wpcf7-form-control-wrap your-email"><input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email" /></span></div>
<div><span class="wpcf7-form-control-wrap subject"><input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Tárgy" /></span></div></div>
<div class="col-md-6">
<div><span class="wpcf7-form-control-wrap your-message"><textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Üzenet"></textarea></span></div>
<div><input type="submit" value="Üzenet Küldése" class="wpcf7-form-control wpcf7-submit" /></div></div></div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></div></div></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid tm_bottom vc_custom_1460539914125 vc_row-has-fill vc_row-o-content-middle vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-3"><div class="vc_column-inner vc_custom_1460539887712"><div class="wpb_wrapper">
	<div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1460539905114  man">
		
		<figure class="wpb_wrapper vc_figure">
			<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="270" height="314" src="../wp-content/uploads/2015/08/man01.png" class="vc_single_image-img attachment-full" alt="man01" srcset="http://woodworker.thememove.com/wp-content/uploads/2015/08/man01.png 270w, http://woodworker.thememove.com/wp-content/uploads/2015/08/man01-258x300.png 258w" sizes="(max-width: 270px) 100vw, 270px" /></div>
		</figure>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-9"><div class="vc_column-inner vc_custom_1460539880591"><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div
	class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center vc_icon_element-have-style">
	<div class="vc_icon_element-inner vc_icon_element-color-white vc_icon_element-have-style-inner vc_icon_element-size-lg vc_icon_element-style-boxed vc_icon_element-background vc_icon_element-background-color-custom" style="background-color:#c69453"><span class="vc_icon_element-icon fa fa-phone" ></span></div>
</div>
<div class="vc_custom_heading vc_custom_1438941083209" ><h4 style="font-size: 14px;color: #594431;text-align: left" >TELEFON</h4></div><div class="vc_custom_heading" ><h4 style="font-size: 15px;color: #333333;text-align: left" ><?php echo $beallitasok->mobil?></h4></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div
	class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center vc_icon_element-have-style">
	<div class="vc_icon_element-inner vc_icon_element-color-white vc_icon_element-have-style-inner vc_icon_element-size-lg vc_icon_element-style-boxed vc_icon_element-background vc_icon_element-background-color-custom" style="background-color:#c69453"><span class="vc_icon_element-icon fa fa-home" ></span></div>
</div>
<div class="vc_custom_heading vc_custom_1438941179840" ><h4 style="font-size: 14px;color: #594431;text-align: left" >CÍM</h4></div><div class="vc_custom_heading" ><h4 style="font-size: 15px;color: #333333;line-height: 1.2;text-align: left" ><?php echo $beallitasok->uzletcim?>
</h4></div></div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div
	class="vc_icon_element vc_icon_element-outer vc_icon_element-align-center vc_icon_element-have-style">
</div>
</div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>
													</div>
						<!-- .entry-content -->
					</article><!-- #post-## -->
												</div>
		</div>
			</div>
</div>

</div><!-- #content -->
    <?php include('footer.php');?>