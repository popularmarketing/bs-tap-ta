<body class="home singular page page-id-4 page-template-default header01 full-width core_132 wpb-js-composer js-comp-ver-4.11.2 vc_responsive">
<div class="snap-drawers">
    <div class="snap-drawer snap-drawer-left">
        <div class="mobile-menu"><ul id="mobile-menu" class="menu">
                <?php foreach ($primari as $row) {
                    $url = base_url("" . $row['url']);
                    if (empty($row['gyerekek'])) {?>
                        <li class="menu-item"><a href="<?php echo $url?>"><?php echo $row['nev']?></a></li>
                    <?php } else { ?>
                        <li class="menu-item menu-item-has-children"><a href=""><?php echo $row['nev']?></a>
                            <ul class="sub-menu">
                                <?php
                                foreach ($row['gyerekek'] as $gyRow) {
                                    $urlChild = base_url("" . $gyRow['url']);
                                    ?>
                                    <li class="menu-item"><a href="<?php echo $urlChild ?>"><?php echo $gyRow['nev'] ?></a></li>
                                <?php }?>
                            </ul>
                        </li>
                    <?php }?>
                <?php }?>
            </ul>
        </div>
    </div>
    <div class="snap-drawer snap-drawer-right"></div>
</div><!-- .snap-drawers -->
<div id="page" class="hfeed site">
    <header class="site-header">
        <div class="site-top hidden-xs hidden-sm">
            <div class="container">
                <div class="row middle">
                    <div class="col-md-12 col-lg-7">
                        <aside id="text-2" class="widget widget_text">			<div class="textwidget"><ul class="extra-info">
                                    <li><i class="fa fa-phone"> </i> <?php echo $beallitasok->mobil?></li>
                                    <li><i class="fa fa-envelope-o"></i> <?php echo $beallitasok->nyilvanosemail?></li>
                                </ul></div>
                        </aside>					</div>
                    <div class="col-lg-5 hidden-md end-lg">
                        <div class="social-menu"><ul id="social-menu-top" class="menu">
                                <li id="menu-item-97" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-97"><a href="http://facebook.com/">facebook</a></li>
                                <li id="menu-item-98" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-98"><a href="http://twitter.com/">twitter</a></li>
                                <li id="menu-item-171" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-171"><a href="http://linkedin.com/">linkedin</a></li>
                                <li id="menu-item-172" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-172"><a href="http://dribbble.com/">dribbble</a></li>
                                <li id="menu-item-173" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-173"><a href="http://youtube.com/">youtube</a></li>
                                <li id="menu-item-174" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-174"><a href="feed/index.html">feed</a></li>
                            </ul></div>					</div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row middle-xs middle-sm">
                <div class="col-md-3 col-xs-10">
                    <div class="site-branding">
                        <a href="fooldal" rel="home"><h1>BS Tapéta Kft.</h1></a>
                    </div>
                </div>
                <div class="col-xs-2 hidden-md hidden-lg end">
                    <i id="open-left" class="fa fa-navicon"></i>
                </div>
                <div class="col-md-9 hidden-xs hidden-sm end">
                    <nav id="site-navigation" class="main-navigation">
                        <div class="container">
                            <div class="row middle">
                                <div class="primary-menu">
                                    <ul id="primary-menu" class="menu">
                                        <?php foreach ($primari as $row) {
                                            $url = base_url("" . $row['url']);
                                            if (empty($row['gyerekek'])) {?>
                                                <li class="menu-item"><a href="<?php echo $url?>"><?php echo $row['nev']?></a></li>
                                            <?php } else { ?>
                                                <li class="menu-item menu-item-has-children"><a href=""><?php echo $row['nev']?></a>
                                                    <ul class="sub-menu">
                                                        <?php
                                                        foreach ($row['gyerekek'] as $gyRow) {
                                                        $urlChild = base_url("" . $gyRow['url']);
                                                        ?>
                                                            <li class="menu-item"><a href="<?php echo $urlChild ?>"><?php echo $gyRow['nev'] ?></a></li>
                                                        <?php }?>
                                                    </ul>
                                                </li>
                                            <?php }?>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <!-- #site-navigation -->
                </div>
            </div>
        </div>
    </header><!-- .site-header -->
