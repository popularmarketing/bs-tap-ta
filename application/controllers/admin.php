<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

       $this->load->library('grocery_CRUD');
       $this->lang->load('default', 'english');
       $this->admincontroll();
       $this->load->model("Lekerdezes");
       $this->load->model("Alapfunction");
       $this->load->model("CallCenterModel");

    }

    public function index()
    {
        $this->kimenet((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
    }

    private function sitepart()
    {
        $adatok = $this->Lekerdezes->beallitasok("");
        $datak = $this->Lekerdezes->felhasznalok();
        $data['oldalnev'] = $adatok->oldalnev;
        $data['avatar'] = $datak->avatar;
        $data['publicname'] = $datak->publicname;

        return $data;
    }

    //Maga a tratalmi rész ahol a crud betölt
    public function kimenet($output = null)
    {

        $adatok = $this->Lekerdezes->beallitasok("");
        $data = $this->Lekerdezes->felhasznalok();
        $output->oldalnev = $adatok->oldalnev;
        $output->avatar = "";
        $output->publicname = "admin";

        $this->load->view('adminpart/header',$output);
        $this->load->view('admin_kimenet.php',$output);


        $this->load->view("adminpart/footer");
    }

    function primari()
    {

    }

    //Fejléc betöltése/csak nem tudom megcsinálni
    public function Adminheader()
    {

        $this->load->view("adminpart/header");

    }

    //Felhasználó kezelése
    public function users()
    {


        $crud = new grocery_CRUD();

        $crud->set_table('users');
        $crud->columns('name','publicname','avatar','perm');
        $crud->display_as('id','Azonosító');
        $crud->display_as('name','Név');
        $crud->display_as('password','Jelszó');
        $crud->display_as('perm','Jogosultság');
        $crud->display_as('avatar','Profilkép');
        $crud->display_as('publicname','Nyilvános név');

        $crud->field_type('perm','dropdown',
            array('1' => 'admin','2' => 'user'));
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $crud->set_field_upload('avatar', 'assets/uploads/files');
        
        
        $crud->callback_before_insert(array($this, '_insert_user'));
        $crud->callback_before_update(array($this, '_insert_user'));
        
        $output = $crud->render();
        $output->oldaltitle = "Felhasználók";


        $this->kimenet($output);
		
    } //Felhasználó kezelése
	
    public function dokumentumok()
    {


        $crud = new grocery_CRUD();

        $crud->set_table('users');
        $crud->columns('name','publicname','avatar','perm');
        $crud->display_as('id','Azonosító');
        $crud->display_as('name','Név');
        $crud->display_as('password','Jelszó');
        $crud->display_as('perm','Jogosultság');
        $crud->display_as('avatar','Profilkép');
        $crud->display_as('publicname','Nyilvános név');

        $crud->field_type('perm','dropdown',
            array('1' => 'admin','2' => 'user'));
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');

        $crud->set_field_upload('avatar', 'assets/uploads/files');
        $output = $crud->render();

        $output->oldaltitle = "Felhasználók";


        $this->kimenet($output);
		
    } //Felhasználó kezelése
 
    //Bolt beállítások
    public function beallitasok()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('beallitasok');


        $crud->display_as(array(
            "oldalnev"=>"Oldal neve",
            "logo"=>"Logó",
            "favicon"=>"Favicon",
            "mobil"=>"Mobil szám",
            "vezetekes"=>"Vezetékes szám",
            "fax"=>"Fax",
            "adoszam"=>"Adószám",
            "telephelycim"=>"Telephely címe",
            "uzletcim"=>"Üzlet címe",
            "nyitvatartas"=>"Nyitva tartás",
            "adminemail"=>"Admin email címe",
            "nyilvanosemail"=>"Megjelenített email",
            "fooldal_title"=>"Főoldali title",
            "fooldal_keywords"=>"Főoldali kulcsszavak",
            "fooldal_description"=>"Főoldali leírás",
            "google_analytics"=>"Google analytics azonosító",
            "nyitva"=>"Nyitva",
            "nyelv"=>"Nyelv"


        ));




        $crud->set_field_upload('favicon', 'assets/uploads/files');
        $crud->set_field_upload('logo', 'assets/uploads/files');


        //$crud->callback_after_upload(array($this, 'resize_after_upload'));


        $select = array(

            1 => "Nyitva",
            0 => "Zárva"
        );

        $crud->field_type('nyitva','dropdown',$select);


       // $tomb = $this->Alapfunction->nyelvek("select");


        $crud->field_type('nyelv', 'readonly');

        $crud->unset_add();
        $crud->unset_delete();

        $output = $crud->render();
        $output->oldaltitle = "Beállítások";
        $this->kimenet($output);
    }

    //Oldalak Létrehozása
    public function oldalak()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('oldalak');


        $crud->display_as(array(
            "nev"=>"Oldal neve",
            "url"=>"Keresőbarát url",
            "szulo"=>"Szülő oldal",
            "cim"=>"Külső url link",
            "sorrend"=>"Sorrend",
            "statusz"=>"Státusz",
            "fokep"=>"Főkép",
            "nyelv"=>"Nyelv",
            "title"=>"SEO title",
            "keywords"=>"SEO kulcsszavak",
            "description"=>"SEO Leírás",
            "header_custom_code"=>"Egyedi fejléc kód"
        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('fokep', 'assets/uploads/files');

        $select = array(  1 => "Bekapcsolva",  0 => "Kikapcsolva"   );
        $crud->field_type('statusz','dropdown',$select);


        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $szulo = $this->Alapfunction->page_to_admin();
        $crud->field_type('szulo', 'dropdown',$szulo);

        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Oldalak";
        $this->kimenet($output);
    } 
	
	//Hírek
    public function hirek()
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('hirek');

	$crud->columns('id','nev','kategoria','videoid','fokep','datum');
	$this->db->order_by('id desc');
		// $crud->set_relation_n_n('id');
        $crud->display_as(array(
            "nev"=>"Cím",
            "url"=>"Keresőbarát url",
            "kategoria"=>"Kategória",
            "lead"=>"lead",
            "tartalom"=>"Tartalom",
			 "datum"=>"Dátum",
            "statusz"=>"Státusz",
            "kiemelt"=>"Kiemelt hír",
            "fokep"=>"Főkép",
            "tag"=>"Tag-ek",		
            "videoid"=>"Videoid",
            "user"=>"Felhasználó",
            "title"=>"SEO title",
            "nyelv"=>"Nyelv",
            "keywords"=>"SEO kulcsszavak",
            "description"=>"SEO Leírás",
            "header_custom_code"=>"Egyedi fejléc kód",
        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('fokep', 'assets/uploads/files');

        $select = array(  1 => "Bekapcsolva",  0 => "Kikapcsolva"   );
        $crud->field_type('statusz','dropdown',$select);
        
        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);
		
		$crud->field_type('kiemelt','true_false');
		
		$nyelv = $this->Alapfunction->hirek_kategoria("select");
		if(!empty($nyelv))
        $crud->field_type('kategoria', 'dropdown',$nyelv);
		
		$nyelv = $this->Alapfunction->admins_to_select("select");
        $crud->field_type('user', 'dropdown',$nyelv);

        $crud->add_action('Shownotes', '', 'admin/shownotes','ui-icon-plus');

        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Hírek";
        $this->kimenet($output);
    }

    //Hírek kategória
    public function hirek_kategoria()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('hirek_kategoria');


        $crud->display_as(array(
            "nev"=>"Cím",
            "url"=>"Keresőbarát url",
            "fokep"=>"Főkép",
            "nyelv"=>"Nyelv"
         
        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('fokep', 'assets/uploads/files');

        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Hírek kategória";
        $this->kimenet($output);
    }
 
    //Kategóriák
    public function kategoria()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('kategoriak');


        $crud->display_as(array(
            "nev"=>"Kategoria neve",
            "url"=>"Keresőbarát url",
            "szulo"=>"Szülő kategoria",
            "sorrend"=>"Sorrend",
            "statusz"=>"Státusz",
            "fokep"=>"Főkép",
            "nyelv"=>"Nyelv",
            "title"=>"SEO title",
            "keywords"=>"SEO kulcsszavak",
            "description"=>"SEO Leírás",
            "header_custom_code"=>"Egyedi fejléc kód"
        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('fokep', 'assets/uploads/files');

        $select = array(  1 => "Bekapcsolva",  0 => "Kikapcsolva"   );
        $crud->field_type('statusz','dropdown',$select);


        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $szulo = $this->Alapfunction->category_to_admin();
        $crud->field_type('szulo', 'dropdown',$szulo);

        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Kategóriák";
        $this->kimenet($output);
    }

    //Márkák pl.: Egy gyártó termékének márkája
    public function marka()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('marka');


        $crud->display_as(array(
            "nev"=>"Márka neve",
            "url"=>"Márka url",
            "sorrend"=>"Sorrend",
            "statusz"=>"Státusz",
            "fokep"=>"Főkép",
            "nyelv"=>"Nyelv",
            "title"=>"SEO title",
            "keywords"=>"SEO kulcsszavak",
            "description"=>"SEO Leírás",
            "header_custom_code"=>"Egyedi fejléc kód"
        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('logo', 'assets/uploads/files');

        $select = array(  1 => "Bekapcsolva",  0 => "Kikapcsolva"   );
        $crud->field_type('statusz','dropdown',$select);


        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);


        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Márkák";
        $output->leiras = "Itt adhatjuk hozzá a gyártó márkáját. Pl egy Airmax, Galaxy, Astra...";
        $this->kimenet($output);
    }

    //Gyártó
    public function gyarto()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('gyarto');


        $crud->display_as(array(
            "nev"=>"Gyártó neve",
            "url"=>"Gyártó url",
            "sorrend"=>"Sorrend",
            "statusz"=>"Státusz",
            "fokep"=>"Főkép",
            "nyelv"=>"Nyelv",
            "title"=>"SEO title",
            "keywords"=>"SEO kulcsszavak",
            "description"=>"SEO Leírás",
            "header_custom_code"=>"Egyedi fejléc kód"
        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('logo', 'assets/uploads/files');

        $select = array(  1 => "Bekapcsolva",  0 => "Kikapcsolva"   );
        $crud->field_type('statusz','dropdown',$select);


        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);


        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Gyártó";
        $output->leiras = "Itt adhatjuk hozzá a gyártó céget. Pl Nike, Samsung, Opel...";
        $this->kimenet($output);
    }

    //Tulajdonság kategória
    public function tulajdonsag_kat()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('tulajdonsag_kat');


        $crud->display_as(array(
            "nev"=>"Tulajdonság kategória neve",
            "nyelv"=>"Nyelv"
        ));


        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);


        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Tulajdonság kategória";
        $output->leiras = "Ha tulajdonságot akarunk létrehozni akkor előbb itt kell meghatároznunk a fő kategóriát pl.: Szín, méret, Anyag.
         Ha ezzel megvagyunk akkor kell a tulajdonságok menüben hozzáadni a kívánt tulajdonságot pl a Szín-hez azt hogy piros vagy a mérethez azt hogy 45 ";
        $this->kimenet($output);
    }

    //Tulajdonságok
    public function tulajdonsag()
    {
        $crud = new grocery_CRUD();


        $crud->set_table('tulajdonsag');


        $crud->display_as(array(
            "nev"=>"Tulajdonság neve",
            "szulo"=>"Szulő kategória",
            "url"=>"URL",
            "nyelv"=>"Nyelv"
        ));


        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $szulo = $this->Alapfunction->tulajdonsag_kat_to_admin();
        $crud->field_type('szulo', 'dropdown',$szulo);



        $crud->callback_before_insert(array($this, 'seo_url'));

        $output = $crud->render();
        $output->oldaltitle = "Tulajdonságok";
        $output->leiras = "Létrehoztuk a tulajdonság kategóriákat így most már itt nyugodtan hozzáadhatunk tulajdonságokat is. Például ha már létrehoztuk a Tulajdonság kategória-nál a Szín-t, akkor itt hozzáadhatjuk a pirosat.";
        $this->kimenet($output);
    }

        // Termék hozzáadása
     public function termekek()
    {

        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');

        $crud->set_table('termekek');
        $crud->columns('nev', 'ar', 'fokep','nyelv');

        
        $crud->display_as(array(
            "nev"=>"Termék neve",
            "url"=>"URL",
			"kategoria"=>"Kategória",
			"leiras"=>"Leírás",
            "fokep"=>"Főkép",
			"ar"=>"Ár",
			"nyelv"=>"Nyelv"
            
        ));


        $crud->fields('nev', 'url','kategoria','leiras','fokep','ar','nyelv');


        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('fokep', 'assets/uploads/termekek');

        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $kat[] = null;
        $kategoriak = $this->db->query("SELECT * FROM kategoriak");
        foreach($kategoriak->result() as $row)
            $kat[$row->id] = $row->nev;

        if(!empty($kat))
            $crud->field_type('kategoria', 'dropdown',$kat);
        else
            $crud->field_type('kategoria', 'hidden');


        $output = $crud->render();
        $output->oldaltitle = "Termék hozzáadás";
        $output->leiras = "";
        $this->kimenet($output);
    }
    public function galeria()
    {

        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');

        $crud->set_table('galeria');
        $crud->columns('nev', 'file');


        $crud->display_as(array(
            "nev"=>"Kép neve",
            "image_url"=>"URL",
            "file"=>"Kép"

        ));

        $crud->fields('nev', 'image_url','file');


        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('file', 'assets/uploads/galeria');



        $output = $crud->render();
        $output->oldaltitle = "Kép hozzáadás";
        $output->leiras = "";
        $this->kimenet($output);
    }

    // Termék tulajdonság hozzáadása
    function termek_tulajdonsagok($id)
    {
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');

        $crud->set_table('termek_tulajdonsagok');
        $crud->where('termek',$id);
        $crud->columns( 'tulajdonsag_id');
        $crud->display_as(array(
            "tulajdonsag_id"=>"Tulajdonság"


        ));

        $szulo = $this->Alapfunction->tulajdonsag_to_admin();
        $crud->field_type('tulajdonsag_id', 'dropdown',$szulo);

        $crud->field_type('termek', 'hidden', $id);


        $output = $crud->render();
        $output->oldaltitle = "Tulajdonság hozzáadása";
        $output->leiras = "";
        $this->kimenet($output);

    }

    // Termék képek rendezése
    function termek_kepek($id)
    {


        $c = new image_CRUD();

        $c->set_table('termek_kepek');
        $c->set_primary_key_field('id');
        $c->set_url_field('file');
        $c->set_ordering_field('sorrend');
        $c->set_relation_field('termek');
        $c->set_title_field('title');
        $c->set_image_path('assets/uploads/termekek');

        //Megtudjuk a termék nevét
        $this->load->model("Lekerdezes");
        $adat = $this->Lekerdezes->termek_lekerdezes(" WHERE id = '$id' ");
        //Űtalakítjuk hogy megfelelő legyen egy kép nevének
        $nev = $this->ekezet($adat->row()->nev);

        $array =array ("admin_termeknev" => $nev);
        $this->session->set_userdata($array);


        $output = $c->render();

        $output->oldaltitle = "Képek hozzáadása";
        $output->leiras = "A főképen kívűl lehetőségünk van még korlátlan számban hozzá adni képeket a websitehoz. Ezeket itt könnyen megtehetjük.<br/>
        A képeket akár sorba is rendezhetjük, vagy címet is adhatunk neki.";
        $this->kimenet($output);
    }

    //Főoldali slideshow
    function slider()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('slider');


        $crud->display_as(array(
            "nev"=>"Elnevezés",
            "url"=>"URL",
            "sorrend"=>"Sorrend",
            "statusz"=>"Státusz",
            "file"=>"Kép",
            "nyelv"=>"Nyelv",

        ));

        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('file', 'assets/uploads/slider');

        $select = array(  1 => "Bekapcsolva",  0 => "Kikapcsolva"   );
        $crud->field_type('statusz','dropdown',$select);

        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $output = $crud->render();
        $output->oldaltitle = "Slideshow képek";
        $output->leiras = "";
        $this->kimenet($output);
    }

    function forditasok()
    {
        $crud = new grocery_CRUD();

        $crud->set_table('nyelvi_forditasok');


        $crud->display_as(array(
            "azonosito"=>"Azonosító",
            "ertek"=>"Érték",
            "file"=>"File",
            "nyelv"=>"Nyelv",


        ));

        $crud->unset_texteditor('ertek','full_text');
        $this->config->set_item('grocery_crud_file_upload_allow_file_types', 'gif|jpeg|jpg|png');
        $crud->set_field_upload('file', 'assets/uploads/forditasok');

        $nyelv = $this->Alapfunction->nyelvek("select");
        $crud->field_type('nyelv', 'dropdown',$nyelv);

        $output = $crud->render();
        $output->oldaltitle = "Fordítások";
        $output->leiras = "Amennyiben nem tudod ez az oldal miért is van , kérlek ne piszkálj bele. Köszönöm!";
        $this->kimenet($output);
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('login'), 'refresh');
    }
    
    function egyeb_oldal_default()
    {
        $data = $this->admin_setup();
        $data['oldaltitle'] = "Tulajdonságok";
        $data['leiras'] = "";
        
        $this->load->view('adminpart/header',$data);
        $this->load->view('admin_kimenet.php',$data);
        
        $this->load->view("adminpart/footer",$data);
    }

    //Egyéb functionök
    function seo_url($post_array)
    {
        $nev = $post_array["nev"] != "" ? $post_array["nev"] : $post_array["cim"] ;
        $nev = $this->ekezet($nev);
        $post_array['url'] = $nev;

        return $post_array;
    }
    
    //A kép teljes elérési útvonalát beletölti az image_url változóba a galéria esetén
    function setFullUrlToGalery($post_array){
        
        $file = $post_array["file"];
        $image_url = "assets/uploads/galeria/".$file;
        $post_array['image_url'] = $image_url;
        

        return $post_array;
    }
    
    function kep_rename($post_array)
    {
        $nev = $post_array["file"];
        $nev = $this->ekezet($nev);
        $post_array['file'] = $nev;


        return $post_array;
    }
    
    function ekezet($txt)
    {
        $ekezetes = array(',','á',' ','Á','É','Í','Ó','Ö','&#213;','Ú','Ü','&#219;','á ','é','í','ó','ö','&#245;','ú','ü','&#251;','ű','Ű ','Ő','ő','lő','ű','á', '!','?');
        $ekezettelen = array('','a','_','a','e','i','o','o','o','u','u','u','a','e','i', 'o','o','o','u','u','u','u','U','o','o','u','a', '', '');
        return strtolower(str_replace($ekezetes, $ekezettelen, $txt));
    }
    
    function resize_after_upload($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        $this->image_moo->load($file_uploaded)->save_pa($prepend="nagy_", $append="", true);
        $this->image_moo->load($file_uploaded)->resize_crop(600,610)->save($file_uploaded, true);

        return true;
    }
    
    function resize_after_upload_nagykep($uploader_response,$field_info, $files_to_upload)
    {
        $this->load->library('image_moo');

        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        $this->image_moo->load($file_uploaded)->resize_crop(570,457)->save($file_uploaded, true);

        return true;
    }
    
    public function _insert_user($value) {
        $value['password'] = md5($value['password']);
        return $value;
    }
    
    private function admincontroll()
    {
        $perm = $this->session->userdata('perm');
        //Ha admin jogokkal bír és be van jelentkezve
        if($perm == 2)
            redirect(base_url('callCenter'), 'refresh');
        else if($perm != 1){
             $hiba = urlencode("Nem vagy bejelentkezve!");
            redirect(base_url('bejelentkezes?par='.$hiba), 'refresh');
        }
    }

    private function admin_setup()
    {
        $adatok = $this->Lekerdezes->beallitasok();
        $adatok2 = $this->Lekerdezes->felhasznalok();
        $data['oldalnev'] = "Popular Marketing";
        $data['avatar'] = "";
        $data['publicname'] = "Admin";
        
        
        return $data;
        }




}

