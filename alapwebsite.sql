-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2016. Ápr 18. 17:24
-- Kiszolgáló verziója: 5.6.29
-- PHP verzió: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beallitasok`
--

CREATE TABLE `beallitasok` (
  `id` int(11) NOT NULL,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'BS Tapéta Kft Debrecen', '2a193-logo.jpg', '', '+36 (52) 310 635', '', '', '', '', '4029 Debrecen Domb u. 4.', '<p>\r\n	H&eacute;tfő - P&eacute;ntek: 9:00 - 17:00 | Szombat: 9:00 - 12:00</p>\r\n', '', 'bstapetakft@upcmail.hu', 1, 'hu', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companytype`
--

CREATE TABLE `companytype` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `dokumentumok`
--

CREATE TABLE `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL,
  `image_url` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `galeria`
--

INSERT INTO `galeria` (`id`, `image_url`, `file`, `nev`, `active`) VALUES
(1, '', '8198e-alma.jpg', 'Alma', 0),
(2, '', 'b0036-szek.jpg', 'Szék', 0),
(3, '', '1d8fc-kanape.jpg', 'Kanapé', 0),
(4, '', 'b5bc6-agy.jpg', 'Ágy', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyarto`
--

CREATE TABLE `gyarto` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyik`
--

CREATE TABLE `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE `hirek` (
  `id` int(11) NOT NULL,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek_kategoria`
--

CREATE TABLE `hirek_kategoria` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria`
--

CREATE TABLE `kategoria` (
  `kategoriaid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoriak`
--

CREATE TABLE `kategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `kategoriak`
--

INSERT INTO `kategoriak` (`id`, `nev`, `url`, `szulo`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(1, 'MODERN TAPÉTÁK', 'modern_tapetak', '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kerdezzfelelek`
--

CREATE TABLE `kerdezzfelelek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marka`
--

CREATE TABLE `marka` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megjelenes`
--

CREATE TABLE `megjelenes` (
  `megjelenesid` int(11) NOT NULL,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nyelvi_forditasok`
--

CREATE TABLE `nyelvi_forditasok` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `oldalak`
--

CREATE TABLE `oldalak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `oldalak`
--

INSERT INTO `oldalak` (`id`, `nev`, `url`, `tartalom`, `szulo`, `cim`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(52, 'Nitólap', 'fooldal', '<div class="wpb_wrapper">\r\n	<div class="vc_custom_heading vc_custom_1438933380841">\r\n		<h2 style="font-size: 30px;color: #333333;text-align: left">\r\n			Rólunk</h2>\r\n	</div>\r\n	<div class="vc_row wpb_row vc_inner vc_row-fluid">\r\n		<div class="wpb_column vc_column_container vc_col-sm-12">\r\n			<div class="vc_column-inner ">\r\n				<div class="wpb_wrapper">\r\n					<div class="wpb_text_column wpb_content_element  grey">\r\n						<div class="wpb_wrapper">\r\n							<p>\r\n								A BS Tapéta Kft. Kelet-Magyarország legnagyobb tapéta választékával várja vásárlóit exkluzív debreceni belvárosi bemutatótermében.\r\n \r\nKözel 10 000 féle terméket kínálunk, modern, design, gyermek, exkluzív és klasszikus stílusokban.\r\n \r\nTapétákon kívül posztereket, kül- és beltéri díszléceket, rejtett világításos díszléceket, valamint szállodai minőségű padlószőnyeget és a hozzá kapcsolódó ragasztókat is forgalmazunk üzletünkben.</p>\r\n							<p>\r\n								A lakossági megrendelések mellett a megyében már számos vendéglátóhely, szálloda fal- és padlóburkolatát választották nálunk. Üzlet- és szállodatulajdonosoknak a felméréstől a kivitelezésig egyaránt rendelkezésére állunk.\r\n \r\nEgy hotel vagy szórakozóhely belső kialakításánál 3D-s látványtervből dolgozunk, mindig a megrendelő egyedi kívánságait szem előtt tartva, együttműködve a belsőépítésszel. \r\n \r\nA régióban egyedülállóan bármilyen mintájú tapétát és padlószőnyeget legyártatunk bizonyos mennyiség felett, a tapéta mintáját kiszerkesztjük és belerakjuk a padlószőnyegbe és viszont.</p>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	<div class="vc_row wpb_row vc_inner vc_row-fluid">\r\n		<div class="wpb_column vc_column_container vc_col-sm-6">\r\n			<div class="vc_column-inner ">\r\n				<div class="wpb_wrapper">\r\n					<div class="wpb_text_column wpb_content_element  grey">\r\n						<div class="wpb_wrapper">\r\n<p>Referencia munkáink:</p>\r\n							<ul class="list">\r\n								<li>\r\n									Hotel Délibáb - Hajdúszoboszló</li>\r\n								<li>\r\n									Hotel Aventinus - Nyíregyháza</li>\r\n								<li>\r\n									Melange kávéház - Debrecen</li>\r\n								<li>\r\n									No1 Bisztro - Debrecen</li>\r\n								<li>\r\n									Gara cukrászda - Debrecen</li>\r\n							</ul>\r\n						</div>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<div class="wpb_column vc_column_container vc_col-sm-6">\r\n			<div class="vc_column-inner ">\r\n				<div class="wpb_wrapper">\r\n					<div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1460531816201">\r\n						<figure class="wpb_wrapper vc_figure">\r\n							<div class="vc_single_image-wrapper   vc_box_border_grey">\r\n								<img alt="img02" class="vc_single_image-img attachment-full" height="200" sizes="(max-width: 320px) 100vw, 320px" src="wp-content/uploads/2015/07/img02.jpg" srcset="http://woodworker.thememove.com/wp-content/uploads/2015/07/img02.jpg 320w, http://woodworker.thememove.com/wp-content/uploads/2015/07/img02-300x188.jpg 300w" width="320" /></div>\r\n						</figure>\r\n					</div>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(53, 'Tapéták', 'tapetak', '', 0, '', 0, 1, '', '', '', '', '', ''),
(54, 'Modern tapéták', 'modern_tapetak', '', 53, '', 0, 1, '', '', '', '', '', ''),
(55, 'Design tapéták', 'design_tapetak', '', 53, '', 1, 1, '', '', '', '', '', ''),
(56, 'Exkluzív tapéták', 'exkluziv_tapetak', '', 53, '', 2, 1, '', '', '', '', '', ''),
(57, 'Klasszikus tapéták', 'klasszikus_tapetak', '', 53, '', 3, 1, '', '', '', '', '', ''),
(58, 'Natrual tapéták', 'natrual_tapetak', '', 53, '', 4, 1, '', '', '', '', '', ''),
(59, 'Amerikai tapéták', 'amerikai_tapetak', '', 53, '', 5, 1, '', '', '', '', '', ''),
(60, 'Gyerek tapéták', 'gyerek_tapetak', '', 53, '', 6, 1, '', '', '', '', '', ''),
(61, 'Tapéta típusok', 'tapeta_tipusok', '', 53, '', 7, 1, '', '', '', '', '', ''),
(63, 'Bemutatóterem', 'bemutatoterem', '', 0, '', 0, 1, '', '', '', '', '', ''),
(64, 'Kapcsolat', 'kapcsolat', '', 0, '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `slider`
--

INSERT INTO `slider` (`id`, `sorrend`, `nev`, `file`, `nyelv`, `url`, `statusz`) VALUES
(1, 0, 'TAPÉTAFAJTÁK <span>EZRÉVEL</span>', '15eb7-s11_bg.jpg', '', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE `termekek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `termekek`
--

INSERT INTO `termekek` (`id`, `nev`, `url`, `kategoria`, `lead`, `leiras`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`, `statusz`, `gyarto`, `marka`, `kiemelt`, `ar`, `ar_akcios`, `valuta`, `ar_beszerzesi`, `suly`, `raktarkeszlet`) VALUES
(1, 'Sajt', '', 1, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kepek`
--

CREATE TABLE `termek_kepek` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_tulajdonsagok`
--

CREATE TABLE `termek_tulajdonsagok` (
  `id` int(11) NOT NULL,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag`
--

CREATE TABLE `tulajdonsag` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag_kat`
--

CREATE TABLE `tulajdonsag_kat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `beallitasok`
--
ALTER TABLE `beallitasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `dokumentumok`
--
ALTER TABLE `dokumentumok`
  ADD PRIMARY KEY (`dokumentumokid`);

--
-- A tábla indexei `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyarto`
--
ALTER TABLE `gyarto`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyik`
--
ALTER TABLE `gyik`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoriaid`);

--
-- A tábla indexei `kategoriak`
--
ALTER TABLE `kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `megjelenes`
--
ALTER TABLE `megjelenes`
  ADD PRIMARY KEY (`megjelenesid`);

--
-- A tábla indexei `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_kepek`
--
ALTER TABLE `termek_kepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `beallitasok`
--
ALTER TABLE `beallitasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `companytype`
--
ALTER TABLE `companytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT a táblához `dokumentumok`
--
ALTER TABLE `dokumentumok`
  MODIFY `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `gyarto`
--
ALTER TABLE `gyarto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyik`
--
ALTER TABLE `gyik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT a táblához `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoriaid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoriak`
--
ALTER TABLE `kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `megjelenes`
--
ALTER TABLE `megjelenes`
  MODIFY `megjelenesid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT a táblához `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT a táblához `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT a táblához `termek_kepek`
--
ALTER TABLE `termek_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
